const endPoints = {
    baseUrl: "https://jsonplaceholder.typicode.com"
}

function fetchData(value, action) {
    try {
        localStorage.getItem("userName");
        document.querySelector("h2");
        fetch(endPoints.baseUrl + value,{method:'GET'})
        .then(function (response) {
            if (response.status !== 200) {
                throw new Error("something went wrong with URL: "+response.url+"<br/>with status: "+response.status);
            }
            response.json().then(function (resp) {
                switch (action) {
                    case "SINGLE":
                        resp = [resp];
                        break;
                    default:
                        break;
                }

                // console.log("response json---> ", resp);
                let dataTag = document.createElement("div");
                let tableData =
                    "<table class='table table-striped table-hover'>" +
                    "<thead>" +
                    "<tr>";

                for (const key in Object.keys(resp[0])) {
                    if (Object.hasOwnProperty.call(Object.keys(resp[0]), key)) {
                        let thead = Object.keys(resp[0])[key];
                        tableData = tableData + "<th scope='col'>" + thead + "</th>";
                    }
                }

                tableData = tableData + "</tr></thead><tbody>";
                for (const element in resp) {
                    if (Object.hasOwnProperty.call(resp, element)) {
                        let oElement = resp[element];
                        tableData = tableData + "<tr>";
                        for (const elem in oElement) {
                            if (Object.hasOwnProperty.call(oElement, elem)) {
                                let singleElem = oElement[elem];
                                let table = "<td>" + singleElem + "</td>";
                                // console.log(typeof(singleElem));
                                if (typeof(singleElem) === 'object' && singleElem !== null && singleElem !== undefined) {
                                    table = "<td>" + JSON.stringify(singleElem) + "</td>";
                                }
                                tableData = tableData + table;
                            }
                        }
                        tableData = tableData + "</tr>";
                    }
                }
                dataTag.innerHTML = tableData + "</tbody></table>";
                document.body.appendChild(dataTag);
            })
        }).catch(function (error) {
            console.log(error);
            let errorElement = document.createElement("div");
            errorElement.innerHTML = "<h4>"+error+"</h4>";
            document.body.appendChild(errorElement);
        });
    } catch (err) {
        console.log(err);
        let errorElement = document.createElement("div");
        errorElement.innerHTML = "<h4>"+err+"</h4>";
        document.body.appendChild(errorElement);
    }
}
