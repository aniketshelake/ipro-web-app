function changeColor(color){
    document.getElementById("colorChange").style.backgroundColor = color;
}

function changeElement(action){
    let changedElem;
    if (action === 'reset') {
        changedElem = "<h5>Please click to change element from h5 to h3</h5>";
    } else {
        changedElem = "<h3>changed! element from h5 to h3</h3>";
    }
    document.getElementById("elemChange").innerHTML = changedElem;
}

function twoWayBind(){
    console.log();
    let input = document.querySelector(".towWayBindInput").value
    document.getElementById("towWayBindOut").innerHTML = "<pre>"+input+"</pre>";

}