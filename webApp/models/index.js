const endPoints = {
    baseUrl: "https://jsonplaceholder.typicode.com"
}
console.log(endPoints);

class GenericUser {
    constructor(userName, userAddress, userContact, userEmailId, password) {
        this.userName = userName;
        this.userAddress = userAddress;
        this.userContact = userContact;
        this.userEmailId = userEmailId;
        this.password = password;
    }
}

class IpUser extends GenericUser {
    constructor(empId, empProfile, empJoinDate, userName, userAddress, userContact, userEmailId, password) {
        super(userName, userAddress, userContact, userEmailId, password);
        this.empId = empId;
        this.empProfile = empProfile;
        this.empJoinDate = empJoinDate;
    }
}

function validateUser(event) {
    event.preventDefault();
    var userName = document.querySelector("#exampleInputEmail1").value;
    var password = document.querySelector("#exampleInputPassword1").value;
    const enteredDate = new Date(password);
    console.log("validateUser called---->", userName, password);
    const currentDate = new Date();
    if (userName === "aniket") {
        if (enteredDate.getDate() === currentDate.getDate()
            && enteredDate.getMonth() === currentDate.getMonth()
            && enteredDate.getFullYear() === currentDate.getFullYear()) {
                localStorage.setItem("userName",userName);
                localStorage.setItem("password",password);
                window.location.href = "file:///home/aniket/web-workspace/ipro-web-app/webApp/views/dashboard.html";
        } else {
            let passwordTag = document.querySelector(".LoginForm");
            let errorMessageTag = document.createElement("span");
            errorMessageTag.setAttribute("class", "wrongPassword");
            errorMessageTag.textContent = "please enter correct password";
            passwordTag.appendChild(errorMessageTag);
        }
    } else {
        let passwordTag = document.querySelector(".LoginForm");
        let errorMessageTag = document.createElement("span");
        errorMessageTag.setAttribute("class", "wrongUsername");
        errorMessageTag.textContent = "please enter correct username";
        passwordTag.appendChild(errorMessageTag);
    }
}


