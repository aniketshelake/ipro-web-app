console.log("asdasd");
const empName = "aniket";
const empId = 1111;
const EMPLOYER = "IP";
console.log("constants variables: " + empName, empId, EMPLOYER);

const literals = "template literals";
console.log(`${literals}:
Look how
It support multiple lines 
and string interpolation`);

const upperCaseName = empName.toUpperCase();
const lowerCaseName = empName.toLowerCase();
const repeated = EMPLOYER.repeat(2);
const replaced = EMPLOYER.replace("p", "pro");
console.log(upperCaseName, lowerCaseName, repeated, replaced);

const address1 = "kolhapur, maharashtra, kolhapur";
const isFromKop = false;
// isFromKop = address1.includes("kolhapur"); //wrong assignment
const isFromPune = false;
// isFromPune = address1.includes("pune"); //wrong assignment
console.log(isFromPune, isFromKop, address1.includes("kolhapur"));

function getLogo(logoName) {
    switch (logoName) {
        case "DASHBOARD":
            return "Go Dash";
            break;
        case "LOGIN":
            return "Go LOGIN";
            break;
        default:
            return "Oops";
            break;
    }
}
const logoName = "DASHBOARD";
const logo = getLogo(logoName);
console.log(logo, logoName === "LOGIN", logoName == "LOGIN");

const fruits = null;
const countq = undefined;
console.log(fruits, countq);


let colors = ["red", "white", "blue"];
for (let i in colors) {
    console.log(colors[i]);
    console.log(i);

}


let person = {
    name: "Tom",
    weight: "150",
    age: 40
};
for (let property in person) {
    console.log("This person's " + property + " is " + person[property] + ".");
}

function addition(param1, param2) {
    //if string 
    console.log(param1 + " " + param2);
    //if numbers
}

addition("sachin", "sehwag");

const arrowFunction = (sourceString, searchString) => {
    sourceString.includes(searchString)
}

function findInString(sourceString, searchString) {
    return sourceString.includes(searchString);
}
console.log("findInString: " + findInString(address1, "kolhapur"));


const days = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"];

for (let index = 0; index < days.length; index++) {
    const day = days[index];
    console.log("day is:" + day);
}

days.push("NA"); //insert at last
days.pop(); //from last
days.shift(); //from first
days.unshift("NA1"); //insert at first
days.slice(1, 3); //from 1 to 3
const split = address1.split(",");
days.join(",");
days.indexOf("WED");
days.includes("WED");
days.reverse();

const ages = [43, 24, 37, 28, 49, 68, 63, 21, 63, 79];
function canRetire(number) {
    return number >= 65;
}
console.log(ages.find(canRetire));

console.log(days);
function isDay(day) {
    return day === "THU";
}
console.log(days.some(isDay));

function foreach(element, index) {
    console.log(index + " ->" + element);
}
days.forEach(foreach);

const names = ["Bill", "Kelly", "John"];
const intros = names.map(function (name) {
    return "My name is " + name;
});
console.log(intros);

const numbers = [1, 2, 3, 4, 5, 6, 7, 8];
const odd = numbers.filter(function (number) {
    return number % 2 == 1;
});
console.log(odd);


const total = numbers.reduce(function (total, number) {
    console.log(total + " " + number);
    return total + number;
});
console.log(total);

// Object -> It is an collection of attributes. It represents a single entity that can manipulate its own data and interact with the rest of your program.
const empolyee = {
    name: "Aniket",
    age: 25,
    profile: function () {
        console.log("------dev----");
    },
};

empolyee.name;
empolyee.age;
empolyee.profile();
empolyee.address = address1;
console.log(empolyee.name + " " + empolyee.age + " " + empolyee.address);
console.log(empolyee);
console.log(empolyee.hasOwnProperty("name"));
console.log(Object.keys(empolyee));
console.log(Object.values(empolyee));

const entries = Object.entries(empolyee);
console.log(entries);

// Classes
// Classes in JavaScript provide a blueprint for the creation of objects. 
// This means that once you define a class (including its member variables and methods), 
// you can endlessly create new objects from it. 
// This is useful because it allows us to reduce the amount of 
// code we need to write when we want multiple objects that are similar to one another.

class Empolyee {
    constructor(name, address, mobile) {
        this.name = name;
        this.address = address;
        this.mobile = mobile;
    }
}

const emp1 = new Empolyee("aniket", address1, "9595545456");
const emp2 = new Empolyee("rahul", address1, "7411741852");
console.log(emp1, emp2);

//inheritance,method overriding,private fields,
class Person {
    _personId = 123;
    #personDt = 123;
    constructor(name, address, mobile) {
        this.name = name;
        this.address = address;
        this.mobile = mobile;
    }
    work() {
        console.log("My name is " + this.name + " and id: " + this._personId + " dt is: " + this.#personDt);
    }
}
class Employee1 extends Person {
    constructor(name, address, mobile, employer) {
        super(name, address, mobile);
        this.employer = employer;
    }
    work() {
        console.log(this.name + " is working in " + this.employer + " company");
    }
    getEmployer() {
        return this.employer;
    }
}
const emp3 = new Employee1("sameer", address1, "345654321", "IP");
emp3.work();
console.log(emp3);
console.log(emp3.getEmployer());

const emp4 = new Person("shubham", address1, "345654321");
emp4.work();
console.log(emp4._personId);
// console.log(emp4.#personDt);//thorws error.

//Set -> only stores unique values
const uniqueDays = new Set();
uniqueDays.add("MON");
uniqueDays.add("MON");
uniqueDays.add("TUE");
uniqueDays.add("WED");
console.log(uniqueDays);
console.log(uniqueDays.size);
console.log(uniqueDays.has("MON"));
uniqueDays.delete("WED");
console.log(uniqueDays);
uniqueDays.clear(); // RIP elements //clean of all of elements
console.log(uniqueDays);

// Map -> stores key-value pairs data
const map = new Map();
map.set("name", "aniket");
map.set("address", address1);
map.set("address", "address1"); //address1(above line) will override by this.
console.log(map)
console.log(map.get("name"));
console.log(map.size);
console.log(map.has("name"));
map.delete("address");
console.log(map)
map.clear(); // RIP elements //clean of all of elements
console.log(map)

//Timeouts and intervals -> Timeouts and intervals allow you to control when and how often a function is invoked.
// Timeouts -> A timeout lets you run a function after a certain amount of time has elapsed.
function sayHello() {
    console.log("Hello!");
}
const timeout = setTimeout(sayHello, 2 * 1000); //It will call after 2sec.
clearTimeout(timeout); // to clear above timeout.

// Intervals -> Intervals are a simpler way of making the same code repeat over and over again, with a set amount of time in between, the interval time.
let seconds = 0;
function printTime() {
    seconds++;
    console.log("Seconds elapsed: " + seconds);

    if (seconds === 10) {
        console.log("10 seconds has elapsed!")
        clearInterval(interval); // to clear above interval.
    }
}
const interval = setInterval(printTime, 100);

// Date
const date1 = new Date();
const date2 = new Date("August 24, 2021");
const date3 = new Date(1000000000000);
const date4 = new Date(2000, 5, 8, 12, 0, 0, 0);//order-> year, month, day, hour, minute, second, and millisecond.
console.log(date1, date2, date3);

/*DOM ->The DOM is a model that describes how pages are structured, 
and how you can manipulate that structure.*/
/* All of what you see that makes up the page is the document. 
Using JavaScript, you can access the elements on the page via the 
document object that is exposed to you by your browser. */

/*
To perform an action when an event triggers, you first need to listen to it. 
Listening to an event is basically telling the browser "hey, when this event occurs, 
I want to execute this piece of code".
That piece of code is packaged in the form of a function called the callback function, 
and is provided to the browser.
*/

function onClickInlineCallback(event) {
    alert("called inline event");
    console.log(event);
}

const emp3Json = JSON.stringify(emp3);
console.log("stringify ", emp3Json);

const json = '{"name":"Sophie","age":3,"weight":20}';
const dog = JSON.parse(json);
console.log("jsonParse", dog);


// AJAX ->AJAX is a technique for sending or requesting data without have to perform a page load.
// AJAX stands for Asynchronous JavaScript and XML. 
const base = 'https://jsonplaceholder.typicode.com';
fetch(base + "/users").then(function (response) {
    // console.log("response---> ",response.json());
    response.json().then(function (json) {
        console.log("response json---> ", json);
        const jResp = document.querySelector(".jsonResp");
        jResp.textContent = JSON.stringify(json);
    })
});

// cookies
function createCookies(key, value) {
    const cookie = key + "=" + value + ";";
    document.cookie = cookie;
}
createCookies("name", "pass123");

let fetchCookies = document.cookie.split('=');
console.log("cookies---->", document.cookie);
for (let i = 0; i < fetchCookies.length; i++) {
    console.log("asdasdasd---->", fetchCookies[i]);
}

// localstorage session
localStorage.setItem("lSession", JSON.stringify(emp1));
const lstorage = localStorage.getItem("lSession");
console.log("lstorage--->", lstorage);

// --------------------------------------------
// Regular expressions -> sequence of characters to be used in a search pattern.

// Meta characters:-------
// .: Any character
// ?: Makes the previous character optional
// \w: A word character
// \W: A non-word character
// \d: A digit
// \D: A non-digit character
// \s: A whitespace character
// \S: A non-whitespace character
// \b: A match at the beginning/end of a word
// \B: A match not at the beginning/end of a word
// \0: A NUL character
// \n: A new line character
// \f: A form feed character
// \r: A carriage return character
// \t: A tab character
// \v: A vertical tab character

// regex6 modifiers:-------
// i: This makes the searching case-insensitive
// g: This makes the searching global which prevents it from stopping after the first match
// m: This makes the searching multiline instead of a single line

// quantifiers:-----------
// +: This repeats the previous character or set one or more times
// *: This repeats the previous character or set zero or more times
// ?: This repeats the previous character or set zero or one time
// {a}: This repeats the previous character exactly a number of times
// {a, b}: This repeats the previous character any number between a and b

stringPattern = "my name is sameer with age of 27; my name is sameer and playing with rahul";
console.log("regex1---->", address1.match(/m/));
console.log("regex2---->", address1.match(/mah./));
console.log("regex3 word char---->", address1.match(/\w/));
console.log("regex4 non word char---->", address1.match(/\W/));
console.log("regex5 whitespace char---->", address1.match(/\s/));
console.log("regex6 modifiers---->", address1.match(/kolhapur/gim));
console.log("regex7 set---->", stringPattern.match(/[123]/));
console.log("regex8 word match1---->", stringPattern.match(/[a-z]/g));
console.log("regex8 word match2---->", stringPattern.match(/[a-z].+/g));
console.log("regex8 word match3---->", stringPattern.match(/[A-Z].+(\.|\?|!)/g));

const regex1 = /^[a-zA-Z0-9.-]{6,12}$/;
console.log("regex9 test()---->", regex1.test(stringPattern));
const regex2 = /(\w+) (\w+) (\w+)/;
console.log("regex10 find and replace---->", stringPattern.replace(regex2, "$3 $2 $1"));
console.log("regex11 find and replace using callback Fun---->", stringPattern.replace(regex2, function (string, group1, group2, group3) {
    return group2 + " " + group3.toUpperCase() + " " + group1;
}));


// call back function: It is a function that is called after another function has completed.
function whichCallback(value, callback) {
    alert(value + "this function should called!");
    callback()
}
function call1() {
    alert("call1 called!");
}
whichCallback("call1 ->", call1);

// promises:-------------
/*
Promises are used to handle the results of an asynchronous operation, 
for example like making an API call or reading from disk. 
We want to continue to allow the code to continue to run and promises 
are a structured way to handle asynchronous operations.
 */
const response = false;
const promisesResp = new Promise((resolve, reject) => {
    if (response) {
        const result = {
            message: "ok---->  promise with success response"
        }
        resolve(result);
    } else {
        const result = {
            message: "failed------> promise with failed response"
        }
        reject(result);
    }
});

promisesResp.then(success => {
    console.log(success.message);
}).catch(error => {
    console.log(error.message);
})

// Async and await:------------
/*
Async and await are keywords in JavaScript that allow you to write promises 
in an easier and more visually appealing manner.
*/

async function successfun1() {
    //todo
    const result = {
        message: "ok---->  promise with success response"
    }
    return result;
}
function successfun2() {
    const result = {
        message: "ok---->  promise with success response"
    }
    return Promise.resolve(result);
}
//successfun1() and successfun2() are same.

async function failedfun1() {
    //todo
    const result = {
        message: "failed------> promise with failed response"
    }
    throw new Error(result);
}
function failedfun2() {
    const result = {
        message: "failed------> promise with failed response"
    }
    return Promise.reject(result);
}
//failedfun1() and failedfun2() are same.

// Await:--------
/*
The await keyword is used with asynchronous functions to ensure that 
all the promises are completed and synchronized. 
This can help remove the need to use callbacks via .then() and .catch().
*/
async function getData(url){
    try {
        // url = "https://www.google.com";
        const response = await fetch(url);
        const respData = await response.json();
        console.log("await resp data---->",respData);
    } catch (error) {
        console.log("await error---->",error);
    }
}
getData("https://jsonplaceholder.typicode.com/todos/1");


var abc="abc";
var abc="asd";
var abc=0;
console.log("abc------->",abc);

{
let ssd="ssd";
const asd = "asda"
console.log("ssd1------->",ssd,asd);
}

let ssd="dds";
const asd = "asd"
console.log("ssd------->",ssd,asd);


